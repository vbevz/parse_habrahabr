use first_parse;

CREATE TABLE `habrahabr` (
    `id` int (3) AUTO_INCREMENT,
    `id_post` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
    `title` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
    `href` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
    `created` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
    `updated` TIMESTAMP,
    PRIMARY KEY (`id`)
);

CREATE TRIGGER tr_habrahabr_updated BEFORE UPDATE ON `habrahabr` FOR EACH ROW
    SET NEW.updated = NOW();

CREATE TRIGGER tr_habrahabr_create BEFORE INSERT ON `habrahabr` FOR EACH ROW 
    SET NEW.created = NOW(), NEW.updated = NOW();
